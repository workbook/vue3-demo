(()=>{
  // 修饰属性
  // class Person{
  //   readonly name:string
  //   constructor(name:string){
  //     this.name = name
  //   }
  //   sayHi(){
  //     console.log('考尼气哇',this.name)
  //   }
  // }

  // const person:Person = new Person('小田田')
  // console.log(person)
  // console.log(person.name)
  // person.name='大田田'
  // console.log(person.name)

  // 修饰构造函数中的参数(参数属性)
  class Person{
    // constructor(readonly name:string){
    // }

    constructor(public name:string){
    }
  }

  const person:Person = new Person('小田田')
  console.log(person)
  console.log(person.name)
})()