(()=>{
  class Person{
    static name1:string = '小田田'
    static sayHi(){
      console.log('hi')
    }
  }

  const person:Person = new Person()
  console.log(Person.name1)
  Person.sayHi()
})()