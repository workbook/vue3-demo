(() => {
  class Person {
    name: string
    age: number
    gender: string
    constructor(name: string='xiaoming', age: number=18, gender: string='男') {
      this.name = name
      this.age = age
      this.gender = gender

    }
    sayHi(str: string) {
      console.log(`我是${this.name}，${str}`)
    }
  }

  class Student extends Person {
    constructor(name: string, age: number, gender: string) {
      super(name, age, gender)
    }
    sayHi(str:string){
      console.log('我是学生类的sayHi方法')
      super.sayHi(str)
    }
  }

  const person = new Person('大明',99,'男')
  person.sayHi('哈哈')

  const stu = new Student('小田',2,'女')
  stu.sayHi('xx')
})()