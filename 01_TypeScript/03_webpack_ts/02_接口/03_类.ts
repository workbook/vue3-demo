(()=>{
  // 类类型
  interface IFly{
    fly()
  }
  class Person implements IFly{
    fly(){
      console.log('我会飞了1')
    }
  }

  const person = new Person()
  person.fly()

  interface ISwim{
    swim()
  }
  class Person2 implements IFly,ISwim{
    fly(){
      console.log('我会飞了2')
    }
    swim(){
      console.log('我会游泳了2')
    }
  }
  const person2 = new Person2()
  person2.fly()
  person2.swim()

  // 接口继承其他接口
  interface IMyFlyAndSwim extends IFly,ISwim{}

  class Person3 implements IMyFlyAndSwim{
    fly(){
      console.log('我会飞了3')
    }
    swim(){
      console.log('我会游泳了3')
    }
  }

  const person3 = new Person3()
  person3.fly()
  person3.swim()
})()