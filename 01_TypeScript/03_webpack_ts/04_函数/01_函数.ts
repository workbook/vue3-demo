(() => {
  console.log('xxx')
  function add(x: number, y: number): number { //命名函数
    return x + y
  }
  const add2 = function (x: number, y: number): number { //函数表达式 匿名函数
    return x + y
  }

  // 函数完整写法
  // add3 ->函数add3
  // (x: number, y: number) => number 当前的这个函数的类型
  const add3: (x: number, y: number) => number = function (x: number, y: number): number {
    return x + y
  }
  console.log(add3(10, 20))

  const add4: (x: number, y: string) => string = function (x: number, y: string): string {
    return x + y
  }
})()