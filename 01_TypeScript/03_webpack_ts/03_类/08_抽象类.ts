(()=>{
  abstract class Animal{
    abstract name:string='小田'
    constructor() {
      
    }
    abstract eat()
    sayHi(){
      console.log('hi')
    }
  }

  class Dog extends Animal{
    name:string = '小黄'
    eat(){
      console.log('eat')
    }
  }

  const dog:Dog = new Dog()
  console.log(dog.name)
  dog.eat()
  dog.sayHi()
})()