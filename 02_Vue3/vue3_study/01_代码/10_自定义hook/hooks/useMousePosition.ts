/*
 * @Author       : hwl
 * @Date         : 2021-05-04 16:40:31
 * @LastEditors  : hwl
 * @LastEditTime : 2021-05-04 16:43:20
 * @FilePath     : \02_Vue3\vue3_study\src\hooks\useMousePosition.ts
 * @Description  :
 */
import { onBeforeUnmount, onMounted, ref } from 'vue'
export default function () {
  const x = ref(-1)
  const y = ref(-1)
  const clickHandler = (event: MouseEvent) => {
    x.value = event.pageX
    y.value = event.pageY
  }
  onMounted(() => {
    window.addEventListener('click', clickHandler)
  })

  onBeforeUnmount(() => {
    window.removeEventListener('click', clickHandler)
  })

  return {
    x,
    y
  }
}
