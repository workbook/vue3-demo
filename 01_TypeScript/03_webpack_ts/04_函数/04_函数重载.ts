(()=>{
  // 函数重载声明
  function add(x:string,y:string):string
  function add(x:number,y:number):number
  function add(x:string|number,y:string|number):string|number{
    if(typeof x === 'string' && typeof y === 'string'){
      return x + y
    }else if(typeof x === 'number' && typeof y === 'number'){
      return x + y
    }
  }
  console.log(add('123','456'))
  console.log(add(123,456))
  // console.log(add('123',456))
  // console.log(add(123,'456'))
})()