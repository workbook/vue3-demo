(() => {
  class Animal {
    name: string
    constructor(name: string) {
      this.name = name
    }
    run(distance: number = 0) {
      console.log(`${this.name}跑了${distance}米`)
    }
  }

  class Dog extends Animal{
    constructor(name:string){
      super(name)
    }
    run(distance:number = 5){
      console.log(`${this.name}跑了${distance}米`)
    }
  }

  class Pig extends Animal{
    constructor(name:string){
      super(name)
    }
    run(distance:number = 5){
      console.log(`${this.name}跑了${distance}米`)
    }
  }

  const ani:Animal = new Animal('动物')

  ani.run()

  const dog:Dog = new Dog('大黄')
  dog.run()

  const pig:Pig = new Pig('佩奇')
  pig.run()

  const dog1:Animal = new Dog('小黄')
  dog1.run()

  const pig1:Animal = new Pig('小猪')
  pig1.run()

  function showRun(ani:Animal){
    ani.run()
  }
  showRun(dog1)
  showRun(pig1)
})()