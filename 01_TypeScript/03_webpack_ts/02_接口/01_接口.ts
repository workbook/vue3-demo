// 接口 
(() => {
  // id只读 sex可选
  interface IPerson {
    readonly id: number
    name: string
    age: number
    sex?: string
  }
  const person: IPerson = {
    id: 1,
    name: '小田田',
    age: 18,
    sex: '男'
  }
})()