(()=>{
  class Person{
    protected name:string
    protected age: number
    public constructor(name:string){
      this.name = name
    }
    public eat(){
      console.log('真好吃',this.name)
    }
  }

  class Student extends Person{
    constructor(name:string){
      super(name)
    }
    play(){
      console.log('玩',this.name)
    }
  }

  const per:Person = new Person('卡卡西')
  // console.log(per.name)
  per.eat()

  const stu:Student = new Student('大黄')
  stu.play()
})()