(() => {
  class Person {
    constructor(public firstName: string, public lastName: string) {

    }
    get fullName() {
      console.log('get')
      return this.firstName +'_'+ this.lastName
    }
    set fullName(str:string){
      console.log('set')
      this.firstName = str.split('_')[0]
      this.lastName = str.split('_')[1]
    }
  }
  const per: Person = new Person('东方', '不拜')
  console.log(per.fullName)
  per.fullName='小_明'
  console.log(per.fullName)
})()