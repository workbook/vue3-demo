/*
 * @Author       : hwl
 * @Date         : 2021-05-04 16:47:54
 * @LastEditors  : hwl
 * @LastEditTime : 2021-05-04 17:41:44
 * @FilePath     : \02_Vue3\vue3_study\src\hooks\useRequest.ts
 * @Description  :
 */
import { ref } from 'vue'
import axios from 'axios'

export default function <T> (url: string) {
  const loading = ref(true)
  const data = ref<T | null>(null)
  const errorMsg = ref('')

  axios.get(url).then(res => {
    loading.value = false
    data.value = res.data
  }).catch(err => {
    loading.value = false
    errorMsg.value = err.message || '未知错误'
  })
  return {
    loading,
    data,
    errorMsg
  }
}
