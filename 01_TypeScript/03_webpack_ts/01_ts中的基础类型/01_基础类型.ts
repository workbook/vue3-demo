(()=>{
  // boolean
  let flag:boolean = true
  console.log(flag)

  // number
  let a1:number = 10 // 10进制
  let a2:number = 0b1010 // 2进制
  let a3:number = 0o12 // 8进制
  let a4:number = 0xa // 16进制
  console.log(a1,a2,a3,a4)

  // string
  let name:string = 'tom'
  name = 'jack'
  const info = `My name is ${name}`
  console.log(info)
  
  // undefined null
  let und:undefined = undefined
  let nll:null = null
  console.log(und,null)

  // 默认情况下 null 和 undefined 是所有类型的子类型。 就是说你可以把 null 和 undefined 赋值给 number 类型的变量。
  let num2:number = undefined // 去掉严格模式就不会报错
  console.log(num2)
  console.log('=====================')

  // array
  let arr1:number[] = [10,2,3] // 定义方式1 数据类型[]
  let arr2:Array<number> = [100,200,300] // 定义方式2 数组泛型
  console.log(arr1,arr2)

  // tuple 元组类型允许表示一个已知元素数量和类型的数组
  // ps: 数量，类型不可变
  let arr3:[string,number,boolean] = ['小田田',18.123,true]
  console.log(arr3)
  console.log(arr3[0].split(''))
  console.log(arr3[1].toFixed(2))
  console.log('=====================')

  // enum enum每个数据值都可以叫元素，元素都有自己的编号，默认从0开始(可以修改)，依次加1 
  enum Color {
    red=1,
    green,
    blue
  }
  let color:Color = Color.red
  console.log(color,Color.red,Color.green,Color.blue)
  console.log(Color[3])

  enum Gender{
    女,
    男
  }
  console.log(Gender.男)

  // any
  let arr:any[] = [100,'小田田',true]
  console.log(arr)
  console.log(arr[1].split(''))

  // void 代表没有返回值
  function showMsg():void{
    console.log('小田田')
    // return
    // return undefined
    return null
  }
  console.log(showMsg())
  console.log('=====================')

  //object
  function getObj(obj: object): object {
    console.log(obj)
    return {
      name: '卡卡西',
      age: 27
    }
  }

  console.log(getObj(undefined))
  console.log(getObj(new String('123')))
  console.log(getObj(String))
  console.log('=====================')

  // 联合类型（Union Types）表示取值可以为多种类型中的一种
  function getString(str:number|string):number {
    // return str.toString().length
    if((<string>str).length){ // 类型断言1
      return (str as string).length // 类型断言2
    }else{
      return str.toString().length
    }
  }

  console.log(getString(''))
  console.log(getString('123'))

  // 类型推断
  let txt = 100 // number
  // txt = '100'
  console.log(txt)

  let txt2 // any
  txt2 = 100
  txt2 = '100'

})()