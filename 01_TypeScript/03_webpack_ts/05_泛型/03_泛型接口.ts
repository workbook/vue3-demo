(() => {

  interface IBaseCRUD<T> {
    data: Array<T>
    add: (t: T) => T
    getUserId: (id: number) => T
  }
  
  class User {
    id?: number
    name: string
    age: number
    constructor(name:string,age:number){
      this.name = name
      this.age = age
    }
  }

  class UserCRUD implements IBaseCRUD<User>{
    data: Array<User> = []
    add(user: User): User {
      user.id = Date.now() + Math.random()
      this.data.push(user)
      return user

    }
    getUserId(id: number): User {
      return this.data.find(user => user.id === id)
    }
  }

  const userCRUD:UserCRUD = new UserCRUD()
  userCRUD.add(new User('jack',20))
  userCRUD.add(new User('tom',25))
  const {id} = userCRUD.add(new User('lucy',23))
  userCRUD.add(new User('rousi',21))
  console.log(userCRUD.data)
  console.log(userCRUD.getUserId(id))
})()