(() => {
  //定义一个接口，用来约束
  interface ILength{
    length:number
  }
  function getLength<T extends ILength>(x: T): number {
    return x.length
  }
  console.log(getLength<string>('what are you no sha le'))
  // console.log(getLength<number>(123))
})()